<?php
/**
 * Created by Steven Jeffries: 9/27/16 12:19 PM
 *
 *
 */

namespace Lib;


use PHPExcel;
use PHPExcel_Writer_Excel2007;
use PHPExcel_Writer_OpenDocument;

class RowWriter {

    private $file;
    private $ext;
    private $header;

    private $wrote_header = false;

    private $fp;

    private $xl;
    private $row_num = 1;
    private $writer;
    private $sheet;


    public function __construct($file) {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $this->file = $file;
        $this->ext = $ext;
    }

    public function set_header($header) {
        $this->header = $header;
    }

    private function write_csv($data) {
        if (!$this->fp) {
            $this->fp = fopen($this->file, 'w');
        }
        fputcsv($this->fp, $data);
    }

    private function write_xlsx($data) {
        if (!$this->writer) {
            $this->xl = new PHPExcel();
            $this->xl->setActiveSheetIndex(0);
            $this->sheet = $this->xl->getActiveSheet();
            $this->writer = new PHPExcel_Writer_Excel2007($this->xl);
        }
        $col = 1;
        foreach ($data as $value) {
            $coord = $this->int_to_xl($col).$this->row_num;
            $this->sheet->setCellValue($coord, $value);
            $col++;
        }
        $this->row_num++;
        $this->writer->save($this->file);
    }

    private function xl_to_int($col) {
        $col = str_split(strtoupper($col));
        $total = 0;
        $pow = 0;
        $a = ord('A');
        for ($i = count($col) - 1; $i >= 0; $i--) {
            $num = ord($col[$i]) - $a + 1;
            $total += pow(26, $pow) * $num;
            $pow++;
        }
        return $total;
    }

    private function int_to_xl($col) {
        $col = base_convert($col, 10, 26);
        $col = strtoupper($col);
        $z = ord('0');
        $n = ord('9');
        $a = ord('A');
        $str = '';
        for ($i = 0; $i < strlen($col); $i++) {
            $num = ord($col[$i]);
            if ($num >= $z && $num <= $n) {
                $num -= $z;
                $num += $a;
                $str .= chr($num - 1);
            } else {
                $num += 10;
                $str .= chr($num - 1);
            }
        }
        return $str;
    }

    private function write_ods($data) {
        if (!$this->writer) {
            $this->xl = new PHPExcel();
            $this->xl->setActiveSheetIndex(0);
            $this->sheet = $this->xl->getActiveSheet();
            $this->writer = new PHPExcel_Writer_OpenDocument($this->xl);
        }
        $col = 1;
        foreach ($data as $value) {
            $coord = $this->int_to_xl($col).$this->row_num;
            $this->sheet->setCellValue($coord, $value);
            $col++;
        }
        $this->row_num++;
        $this->writer->save($this->file);
    }

    private function write_contents($data) {
        if ($this->ext === 'csv') {
            $this->write_csv($data);
        } elseif ($this->ext === 'xlsx') {
            $this->write_xlsx($data);
        } elseif ($this->ext === 'ods') {
            $this->write_ods($data);
        }
    }

    public function write_row($data) {
        if ($this->header && !$this->wrote_header) {
            $this->write_contents($this->header);
            $this->wrote_header = true;
        }
        $this->write_contents($data);
    }

    public function close() {
        if ($this->ext === 'csv') {
            fclose($this->fp);
        } elseif ($this->ext === 'xlsx') {

        } elseif ($this->ext === 'ods') {
            $this->writer->save($this->file);
        }
    }

}