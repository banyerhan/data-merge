<?php

if (!defined('SEAN_PROJECT_HELPER_FUNCTIONS')) {

    define('SEAN_PROJECT_HELPER_FUNCTIONS', true);

    function session($var, $to_set = 'SEAN_PROJECT_SET') {
        if ($to_set !== 'SEAN_PROJECT_SET') {
            $_SESSION[$var] = $to_set;
        } elseif (array_key_exists($var, $_SESSION)) {
            return $_SESSION[$var];
        }
        return null;
    }

    function println($msg) {
        echo "$msg\n";
    }

    function cmp_format($val) {
        return preg_replace('#[^a-z]#', '', strtolower($val));
    }

    function post($var, $to_set = 'SEAN_PROJECT_SET') {
        if ($to_set != 'SEAN_PROJECT_SET') {
            return $_POST[$var] = $to_set;
        } elseif (array_key_exists($var, $_POST)) {
            return $_POST[$var];
        }
        return null;
    }

    function lock($name, $func) {
        $lock_dir = dirname(__DIR__).'/locks';
        while (!@mkdir("$lock_dir/$name", 0777, true)) {
            usleep(200);
        }
        try {
            $func();
        } finally {
            rmdir("$lock_dir/$name");
        }
    }

}