<?php

namespace Lib;

/**
 * Created by Steven Jeffries: 9/21/16 8:56 PM
 *
 *
 */
class RowReader {

    private $filename;
    private $type;

    private $header = null;

    private $excel_reader;
    private $excel;
    private $excel_sheet;
    private $width;
    private $height;
    private $current_row;

    private $num_rows;

    private $fp;

    public function __construct($file) {
        $this->filename = $file;
        $this->current_row = 0;
        $this->type = pathinfo($file, PATHINFO_EXTENSION);
    }

    private function next_csv() {
        if ($this->fp === null) {
            $this->fp = fopen($this->filename, 'r');
        }
        if ($row = @fgetcsv($this->fp)) {
            return $row;
        } else {
            @fclose($this->fp);
            return false;
        }
    }

    private function xl_to_int($col) {
        $col = str_split(strtoupper($col));
        $total = 0;
        $pow = 0;
        $a = ord('A');
        for ($i = count($col) - 1; $i >= 0; $i--) {
            $num = ord($col[$i]) - $a + 1;
            $total += pow(26, $pow) * $num;
            $pow++;
        }
        return $total;
    }

    private function int_to_xl($col) {
        $col = base_convert($col, 10, 26);
        $col = strtoupper($col);
        $z = ord('0');
        $n = ord('9');
        $a = ord('A');
        $str = '';
        for ($i = 0; $i < strlen($col); $i++) {
            $num = ord($col[$i]);
            if ($num >= $z && $num <= $n) {
                $num -= $z;
                $num += $a;
                $str .= chr($num - 1);
            } else {
                $num += 10;
                $str .= chr($num - 1);
            }
        }
        return $str;
    }

    private function next_xl() {
        if ($this->excel === null) {
            $this->excel_reader = \PHPExcel_IOFactory::createReaderForFile($this->filename);
            $this->excel = $this->excel_reader->load($this->filename);
            $this->excel->setActiveSheetIndex(0);
            $this->excel_sheet = $this->excel->getActiveSheet();
            $this->width = $this->excel_sheet->getHighestColumn();
            $this->width = $this->xl_to_int($this->width);
            $this->height = $this->excel_sheet->getHighestRow();
        }
        if ($this->current_row++ < $this->height) {
            $row = [];
            for ($col = 0; $col < $this->width; $col++) {
                $cell = $this->excel_sheet->getCellByColumnAndRow($col, $this->current_row);
                $row[] = $cell->getValue()."";
            }
            return $row;
        }
        return false;
    }

    public function next_row() {
        if ($this->type === 'csv') {
            $row = $this->next_csv();
        } else {
            $row = $this->next_xl();
        }
        if ($row) {
            if ($this->header) {
                $arr = [];
                for ($i = 0; $i < count($this->header); $i++) {
                    $arr[$this->header[$i]] = $row[$i];
                }
                return $arr;
            } else {
                return $row;
            }
        } else {
            return false;
        }
    }

    public function create_header() {
        $this->header = $this->next_row();
    }

    public function get_header() {
        return $this->header;
    }

}