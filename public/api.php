<?php

ignore_user_abort(true);
set_time_limit(0);

require '../init.php';

$json = null;
if ($method = post('method')) {
    if ($method === 'check_status') {
        if ($key = post('key')) {
            $dir = dirname(__DIR__)."/working/$key";
            lock($key, function() use($key, $dir, &$json) {
                if (file_exists("$dir/_current_status")) {
                    $json = json_decode(file_get_contents("$dir/_current_status"), true);
                } else {
                    touch("$dir/_current_status");
                    shell_exec('nohup php "'.(dirname(__DIR__).'/start_merge.php').'" "'.$key.'" > foobar 2>foobarerr &');
                    $json = [
                        'percent' => 0,
                        'error' => false,
                        'complete' => false,
                        'message' => 'Merge process started.',
                        'file_path' => null
                    ];
                }
            });
        } else {
            $json = [
                'percent' => 0,
                'error' => true,
                'message' => 'Key needed for method "check_status".'
            ];
        }
    } else {
        $json = [
            'percent' => 0,
            'error' => true,
            'message' => "Unknown method $method."
        ];
    }
} else {
    $json = [
        'percent' => 0,
        'error' => true,
        'message' => 'API method not given.'.json_encode($_POST)
    ];
}

echo json_encode($json);
exit();