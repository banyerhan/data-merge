<?php


require '../init.php';

$key = $_GET['key'];

$ses = dirname(__DIR__) . "/working/$key/_session_contents";
if (file_exists($ses)) {
    $ses = file_get_contents($ses);
    $ses = unserialize($ses);
    $file = $ses['options']['file']['name'] . '.' . $ses['options']['file']['ext'];
    $exists = true;
} else {
    $file = null;
    $exists = false;
}

?>
<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/tether.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/tether-theme-basic.min.css"/>
    <link rel="stylesheet" type="text/css" href="/ui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="/ui/jquery-ui.structure.min.css"/>
    <link rel="stylesheet" type="text/css" href="/ui/jquery-ui.theme.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>

    <link rel="stylesheet" type="text/css" href="/css/style.css"/>

    <script src="/js/jquery.js"></script>
    <script src="/ui/jquery-ui.min.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <?php if (!$exists) { ?>
        <script src="/js/easeljs.min.js"></script><?php } ?>
    <script src="/js/script.js"></script>
    <!--
      Created by:

      ███████╗████████╗███████╗██╗   ██╗███████╗███╗   ██╗         ██╗███████╗███████╗███████╗██████╗ ██╗███████╗███████╗
      ██╔════╝╚══██╔══╝██╔════╝██║   ██║██╔════╝████╗  ██║         ██║██╔════╝██╔════╝██╔════╝██╔══██╗██║██╔════╝██╔════╝
      ███████╗   ██║   █████╗  ██║   ██║█████╗  ██╔██╗ ██║         ██║█████╗  █████╗  █████╗  ██████╔╝██║█████╗  ███████╗
      ╚════██║   ██║   ██╔══╝  ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║    ██   ██║██╔══╝  ██╔══╝  ██╔══╝  ██╔══██╗██║██╔══╝  ╚════██║
      ███████║   ██║   ███████╗ ╚████╔╝ ███████╗██║ ╚████║    ╚█████╔╝███████╗██║     ██║     ██║  ██║██║███████╗███████║
      ╚══════╝   ╚═╝   ╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝     ╚════╝ ╚══════╝╚═╝     ╚═╝     ╚═╝  ╚═╝╚═╝╚══════╝╚══════╝
      -->
</head>
<body>
<?php if ($exists): ?>
    <div class="container">
        <div class="jumbotron">
            <h3 class="display-5 text-center"><?php echo $file; ?></h3>
            <br/>
            <div class="text-xs-center" id="progress-caption">&nbsp;</div>
            <progress class="progress" value="50" max="100" aria-describedby="progress-caption"
                      id="progress"></progress>
        </div>

        <div class="row marketing" id="merge_progress">
            <div class="col-md-12">
                <h1>File Merging Progress</h1>
                <p>Your files will continue to merge together even if you close the page now.</p>
                <p>However, if you don't keep the link here, you won't be able to access the merged file.</p>
                <p>Save this url: <code
                        id="save_url"><?php echo "http://" . $_SERVER['HTTP_HOST'] . '/working.php?key=' . $key; ?></code>
                </p>
            </div>
        </div>

        <div class="row marketing" id="hidden_div">
            <div class="col-md-12 hidden">
                <h1>File is Complete!</h1>
                <p>Congrats! The files have finished being merged together. Download your file below:</p>
                <p><a href="#" download id="download_link"><?php echo $file; ?></a></p>
                <p>This link will only be available for 24 hours, so make sure to grab it while you have the chance.</p>
            </div>
        </div>
    </div>
    <style>
        progress {
            border: 1px solid #ccc !important;
        }

        code {
            cursor: pointer;
        }
    </style>
    <script>

        $(document).ready(function () {
            $('#hidden_div').hide();
            $('.hidden').removeClass('hidden');

            var waiting_response = false;
            var completed = false;
            var interval = false;

            var check_status = function () {
                if (!waiting_response && !completed) {
                    // Prevents excess api calls if they are slow.
                    waiting_response = true;
                    $.post("/api.php", {method: 'check_status', key: '<?php echo $key; ?>'}, function (data) {
                        data = JSON.parse(data);
                        console.log(data);
                        if (data.complete || data.error) {
                            window.clearInterval(interval);
                        }
                        var p = parseInt(data.percent);
                        $('#progress').attr('value', data.percent);
                        $('#progress-caption').text(data.message + ' (' + p + '%)');
                        waiting_response = false;
                        if (data.complete) {
                            $('#download_link').attr('href', '/exports/' + data.file_path);
                            $('#merge_progress').hide(500, function () {
                                $('#hidden_div').show(500);
                            });
                            completed = true;
                        }
                    });
                }
            };

            var select_text = function (element) {
                var doc = document
                    , text = doc.getElementById(element)
                    , range, selection
                    ;
                if (doc.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText(text);
                    range.select();
                } else if (window.getSelection) {
                    selection = window.getSelection();
                    range = document.createRange();
                    range.selectNodeContents(text);
                    selection.removeAllRanges();
                    selection.addRange(range);
                }
            };

            $('#save_url').click(function () {
                select_text('save_url');
                document.execCommand("copy");
            });


            $('button').click(function () {
                check_status();
            });

            interval = window.setInterval(check_status, 500);
        });
    </script>
<?php else: ?>
<?php

if ($chuck = @file_get_contents('http://api.icndb.com/jokes/random')) {
    $chuck = json_decode($chuck, true);
    $chuck = $chuck['value'];
}

?>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-3">Invalid/Expired Key</h1>
            <p class="lead">Sorry, they key <code><?php echo $key; ?></code> is either invalid or expired.</p>
            <p>You can do the <a href="/">export again</a> any time you want though!</p>
        </div>
        <?php if ($chuck): ?>
            <div class="row marketing">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Since you can't get your file, enjoy a Chuck Norris fact from <a href="http://www.icndb.com/">http://www.icndb.com/</a>
                        </div>
                        <div class="card-block">
                            <h4 class="card-title">Chuck Norris Fact #<span
                                    id="fact-id"><?php echo $chuck['id']; ?></span></h4>
                            <p class="card-text" id="fact-holder"><?php echo $chuck['joke']; ?></p>
                            <button class="btn btn-primary" id="chuck-but">Get another one!</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($chuck): ?>
        <script>
            $(document).ready(function () {
                $('#chuck-but').click(function () {
                    var but = $(this);
                    but.addClass('disabled').prop('disabled', true).text('Humbly begging Mr. Norris for more facts...');
                    $.getJSON('http://api.icndb.com/jokes/random', function (data) {
                        data = data.value;
                        $('#fact-id').text(data.id);
                        $('#fact-holder').html(data.joke);
                        but.removeClass('disabled').prop('disabled', false).text('Get another one!');
                    });
                });
            });
        </script>
    <?php endif; ?>
<?php endif; ?>
</body>
</html>