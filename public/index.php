<?php

require '../init.php';

$step = intval(session('step'));
if ($step === 0) {
    session('step', $step = 1);
}

define('SEAN_PROJECT_STEP', $step);

if (isset($_GET['step'])) {
    session('step', intval($_GET['step']));
    header('Location: /');
} elseif (isset($_GET['clear']) && $_GET['clear'] === 'clear') {
    session_unset();
    header('Location: /');
} elseif (isset($_POST['step_submit'])) {
    require dirname(__DIR__).'/pages/submit_'.SEAN_PROJECT_STEP.'.php';
    session('step', session('step') + 1);
    header('Location: /');
} else {

    require dirname(__DIR__) . '/pages/header.php';

    if (file_exists($page = dirname(__DIR__) . '/pages/step_' . SEAN_PROJECT_STEP . '.php')) {
        /** @noinspection PhpIncludeInspection */
        require $page;
    }

    require dirname(__DIR__) . '/pages/footer.php';
}