var step_1 = {};

$(document).ready(function () {

    step_1 = new (function () {

        var me = this;
        var form = $('form#step_1_form');
        var more = $('#step_1_form_more');
        var file_template = '<div class="step_1_file">' +
            '<input type="file" accept=".xls,.xlsx,.csv" name="step_1_file[]" id="step_1_file[]"/>' +
            '<a href="#" class="remove btn btn-danger">x</a></div>';

        var file_count = 0;

        var more_update = function() {
            more.find('a').click(function(e) {
                me.add_file();
                e.preventDefault();
            });
        };

        var update_file_count = function() {
            file_count = 0;
            form.find('input:file').each(function() {
                if ($(this).val()) {
                    file_count++;
                }
            });
            if (file_count >= 2) {
                $("#step_1_next").text("Use these " + file_count + " files").prop('disabled', false);
            } else {
                $("#step_1_next").text("Select 2 or more files").prop('disabled', true);
            }
        };

        this.add_file = function () {
            more.remove();
            var file = $(file_template);
            form.append(file).append(more);
            file.find('.remove').click(function(e) {
                file.remove();
                e.preventDefault();
                update_file_count();
            });
            file.find('input:file').change(update_file_count);
            more_update();
        };


        $("#step_1_next").click(function(e) {
            e.preventDefault();
            if (file_count >= 2) {
                form.submit();
            } else {
                $("#step_1_info").text("Must select at least 2 files.");
            }
        });


        this.add_file();
        this.add_file();

    })();

});

