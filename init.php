<?php

if (!defined('SEAN_PROJECT_INIT')) {

    define('SEAN_PROJECT_INIT', true);

    define('TMP_DIR', dirname(__FILE__) . '/tmp');

    spl_autoload_register(function ($class) {
        $class = explode('\\', $class);
        if (count($class)) {
            if ($class[0] === 'Lib') {
                array_shift($class);
                require dirname(__FILE__) . '/lib/' . join($class, '/') . '.php';
            }

        }
    });

    if (php_sapi_name() !== 'cli') {
        session_start();
    }

    require dirname(__FILE__) . '/lib/helpers.php';
    require dirname(__FILE__).'/lib/PHPExcel/PHPExcel.php';
}