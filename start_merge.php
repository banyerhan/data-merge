<?php

use Lib\RowReader;
use Lib\RowWriter;

ignore_user_abort(true);
set_time_limit(0);

require 'init.php';

function update_results($perc, $message) {
    global $argv;
    $dir = dirname(__FILE__) . '/working/' . $argv[1];
    lock($argv[1], function () use ($perc, $message, $dir) {
        $json = [
            'percent' => $perc,
            'error' => false,
            'complete' => false,
            'message' => $message,
            'file_path' => null
        ];
        file_put_contents("$dir/_current_status", json_encode($json));
    });
    println(intval($perc) . "%:\t$message");
}

function err($message) {
    // TODO error handling.
    die($message);
}


if ($key = $argv[1]) {

    update_results(0, "Merge process started. Setting up environment.");
    //     _____             _____      _
    //    |  ___|           /  ___|    | |
    //    | |__ _ ____   __ \ `--.  ___| |_ _   _ _ __
    //    |  __| '_ \ \ / /  `--. \/ _ \ __| | | | '_ \
    //    | |__| | | \ V /  /\__/ /  __/ |_| |_| | |_) |
    //    \____/_| |_|\_(_) \____/ \___|\__|\__,_| .__/
    //                                           | |
    //                                           |_|
    /////////////////////////////////////////////////////////////////////

    /*
     * Initializing Variables
     */
    $dir = dirname(__FILE__) . '/working/' . $key;
    $ses = file_get_contents("$dir/_session_contents");
    $ses = unserialize($ses);
    $options = $ses['options'];

    /*
     * Setting up the files array to be easier to work with for this purpose.
     */
    $tmp_files = $ses['files'];
    $files = [];
    foreach ($tmp_files as $tmp_file) {
        $file = [
            'name' => $tmp_file['name'],
            'ext' => $tmp_file['ext'],
            'file' => $tmp_file['tmp_name'],
            'mapping' => []
        ];
        $files[$tmp_file['id']] = $file;
    }
    unset($tmp_files, $tmp_file, $file);


    /*
     * Setting up the mapping to how I want to use it for this purpose.
     */
    $mapping = $ses['mapping'];
    $matches = [];
    foreach ($mapping as $key => $arr) {
        if (!$arr['deleted']) {
            if ($arr['match']) {
                $matches[] = [
                    'key' => $key,
                    'name' => $arr['name'],
                    'type' => $arr['match']
                ];
            }
            foreach ($arr['files'] as $id => $field) {
                $files[$id]['mapping'][$field] = $arr['name'];
            }
        }
    }
    unset($key, $arr, $id, $field);

    /*
     * Create the DB being used for checking collisions and building rows; add the fields to it.
     */
    @unlink("$dir/_database.sqlite");
    $db = new SQLite3("$dir/_database.sqlite");
    $fields = [
        'ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL'
    ];
    foreach (array_keys($files) as $file_id) {
        $fields[] = "file_$file_id TEXT";
    }
    foreach ($matches as $arr) {
        $fields[] = "match_{$arr['key']} TEXT NOT NULL";
    }
    $query = "CREATE TABLE matcher (\n" . join(",\n", $fields) . "\n);";
    if (!$db->exec($query)) {
        err('db error');
    }

    /*
     * Just figuring out how many total rows there are so that the user can be updated with a
     * progress percentage.
     */
    $total_rows = 0;
    foreach ($files as &$file) {
        $reader = new RowReader($file['file']);
        $reader->create_header();
        $num_rows = 0;
        while ($reader->next_row()) {
            $num_rows++;
        }
        $reader = new RowReader($file['file']);
        $reader->create_header();
        $file['reader'] = $reader;
        $file['num_rows'] = $num_rows;
        $total_rows += $num_rows;
        unset($file);
    }

    update_results(10, "Initial data loaded. Beginning import of $total_rows rows.");
    //    ______      _          _____                           _
    //    |  _  \    | |        |_   _|                         | |
    //    | | | |__ _| |_ __ _    | | _ __ ___  _ __   ___  _ __| |_
    //    | | | / _` | __/ _` |   | || '_ ` _ \| '_ \ / _ \| '__| __|
    //    | |/ / (_| | || (_| |  _| || | | | | | |_) | (_) | |  | |_
    //    |___/ \__,_|\__\__,_|  \___/_| |_| |_| .__/ \___/|_|   \__|
    //                                         | |
    //                                         |_|
    /////////////////////////////////////////////////////////////////////////////////

    $row_count = 0;
    foreach ($files as $file_id => $file) {
        /** @var RowReader $reader */
        $reader = $file['reader'];
        $mapping = $file['mapping'];
        while ($row = $reader->next_row()) {

            // Map the contents from this file's row into the master map.
            $mapped = [];
            foreach ($mapping as $field => $map) {
                $mapped[$map] = $row[$field];
            }
            unset($field, $map);

            // Builds queries to insert comparison columns in the DB.
            $queries = [];
            $values = [];
            foreach ($matches as $match) {
                $type = $match['type'];
                $value = trim($mapped[$match['name']]);
                if ($type === 'exact') {
                    $value = $value;
                } elseif ($type === 'case_insensitive') {
                    $value = strtolower($value);
                } elseif ($type === 'frugal') {
                    $value = preg_replace('#[^0-9a-z]#', '', strtolower($value));
                } elseif ($type === 'numbers_only') {
                    $value = preg_replace('#[^0-9]#', '', $value);
                } else {
                    err("Unknown match type $type.");
                }
                $values[$match['key']] = $value;
                $queries[] = 'match_' . $match['key'] . ' = :' . $match['key'];
            }

            // Sees if there is already a matching row in the DB
            $query = 'SELECT ID FROM matcher WHERE ' . join(' AND ', $queries) . ';';
            $stm = $db->prepare($query);
            foreach ($values as $key => $value) {
                $stm->bindValue(":$key", $value);
            }
            $res = $stm->execute();

            // If there is a matching row, update it. Otherwise add this files contents to the DB.
            if ($res = $res->fetchArray(SQLITE3_ASSOC)) {
                $id = $res['ID'];
                $query = "UPDATE matcher SET file_$file_id = :file WHERE ID = $id";
                $stm = $db->prepare($query);
                $stm->bindValue(':file', serialize($mapped));
                $stm->execute();
            } else {
                $query = 'INSERT INTO matcher (file_' . $file_id . ', ' . join(', ', preg_filter('#^#', 'match_', array_keys($values))) . ') VALUES (:file, ' . join(', ', preg_filter('#^#', ':', array_keys($values))) . ');';
                $stm = $db->prepare($query);
                foreach ($values as $key => $value) {
                    $stm->bindValue(":$key", $value);
                }
                $stm->bindValue(':file', serialize($mapped));
                $stm->execute();
            }

            // Update percentages for the user.
            $row_count++;
            $perc = $row_count / $total_rows;
            $perc *= .4;
            $perc *= 100;
            $perc += 10;
            update_results($perc, "Imported row $row_count / $total_rows");
        }
    }

    update_results(50, "Import complete. Exporting data.");
    //    ______      _          _____                      _
    //    |  _  \    | |        |  ___|                    | |
    //    | | | |__ _| |_ __ _  | |____  ___ __   ___  _ __| |_
    //    | | | / _` | __/ _` | |  __\ \/ / '_ \ / _ \| '__| __|
    //    | |/ / (_| | || (_| | | |___>  <| |_) | (_) | |  | |_
    //    |___/ \__,_|\__\__,_| \____/_/\_\ .__/ \___/|_|   \__|
    //                                    | |
    //                                    |_|
    //////////////////////////////////////////////////////////////

    $export_file = "$dir/{$options['file']['name']}.{$options['file']['ext']}";
    $writer = new RowWriter($export_file);
    $header = [];
    foreach ($ses['mapping'] as $key => $arr) {
        if (!$arr['deleted']) {
            $header[] = $arr['name'];
        }
    }
    $writer->set_header($header);
    $limit = 100;
    $offset = 0;
    $rows = $db->querySingle('SELECT COUNT(1) FROM matcher');
    $match_type = $options['match']['type'];
    $match_option = $options['match']['option'];
    $row_count = 0;
    while ($offset < $rows) {
        $result = $db->query("SELECT * FROM matcher LIMIT $offset, $limit");
        $offset += $limit;
        while ($db_row = $result->fetchArray(SQLITE3_ASSOC)) {

            $file_cont = [];
            foreach ($files as $id => $file) {
                if ($db_row["file_$id"]) {
                    $file_cont[$id] = unserialize($db_row['file_' . $id]);
                }
            }

            if (count($file_cont) > 1) {
                if ($match_type === 'merge') {
                    $final_row = [];
                    foreach ($file_cont as $id => $cont) {
                        foreach ($cont as $key => $value) {
                            if (!array_key_exists($key, $final_row)) {
                                $final_row[$key] = [];
                            }
                            if (trim($value)) {
                                if (!in_array($value, $final_row[$key])) {
                                    $final_row[$key][] = $value;
                                }
                            }
                        }
                    }
                    foreach ($final_row as $key => $arr) {
                        $final_row[$key] = join($match_option, $arr);
                    }
                } elseif ($match_type === 'mark') {
                    $final_row = [];
                    $first_id = $first = null;
                    foreach ($file_cont as $id => $arr) {
                        $first_id = $id;
                        $first = $arr;
                        unset($file_cont[$id]);
                        break;
                    }
                    foreach ($first as $key => $value) {
                        if (trim($value)) {
                            $final_row[$key] = [$first_id => $value];
                        } else {
                            $final_row[$key] = [];
                        }
                        foreach ($file_cont as $id => $arr) {
                            if (trim($arr[$key])) {
                                $final_row[$key][$id] = $arr[$key];
                            }
                        }
                    }
                    foreach ($final_row as $key => $arr) {
                        if (count($arr)) {
                            if (count($unique = array_unique($arr)) == 1) {
                                $final_row[$key] = array_pop($unique);
                            }
                        } else {
                            $final_row[$key] = '';
                        }
                    }
                    foreach ($final_row as $key => $arr) {
                        if (is_array($arr)) {
                            $items = [];
                            foreach ($arr as $file_id => $value) {
                                $name = $files[$file_id]['name'];
                                $new_val = str_replace('%FILE_ID%', $file_id, $match_option);
                                $new_val = str_replace('%FILE_NAME%', $name, $new_val);
                                $new_val = str_replace('%CONTENTS%', $value, $new_val);
                                $items[] = $new_val;
                            }
                            $final_row[$key] = join("\n", $items);
                        }
                    }

                } elseif ($match_type === 'override') {
                    $final_row = [];
                    foreach ($match_option as $file_id) {
                        $arr = $file_cont[$file_id];
                        foreach ($arr as $key => $value) {
                            if (trim($value) && !array_key_exists($key, $final_row)) {
                                $final_row[$key] = $value;
                            }
                        }
                    }
                }
            } else {
                $final_row = array_pop($file_cont);
            }
            $writer->write_row($final_row);
            $perc = $row_count / $rows;
            $perc *= .5;
            $perc *= 100;
            $perc += 50;
            update_results($perc, "Writing row $row_count / $rows");
            $row_count++;
        }
    }
    update_results(99, "Saving file...");
    $writer->close();


    $export_dir = dirname(__FILE__).'/public/exports/'.$ses['unique_key'];
    @mkdir($export_dir, 0777, true);
    copy($export_file, $export_dir."/{$options['file']['name']}.{$options['file']['ext']}");
    $dir = dirname(__FILE__) . '/working/' . $argv[1];
    lock($argv[1], function () use ($dir, $ses, $options) {
        $json = [
            'percent' => 100,
            'error' => false,
            'complete' => true,
            'message' => "File is complete.",
            'file_path' => $ses['unique_key']."/{$options['file']['name']}.{$options['file']['ext']}"
        ];
        file_put_contents("$dir/_current_status", json_encode($json));
    });

} else {
    err("Key must be given in order to start the merging of files.");
}



/**
 * TODO List:
 * 1 - Somehow get formatted cell contents so that files can go from xl to xl and keep text formatting.
 */