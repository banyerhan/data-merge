<?php

$files = session('files');
$mapping = session('mapping');
$has_matching = false;
foreach ($mapping as $key => $row) {
    if ($row['match']) {
        $has_matching = true;
        break;
    }
}

?>

<!--suppress CssUnusedSymbol -->
<div class="jumbotron">
    <h1 class="display-3">Step 3: Options</h1>
    <p class="lead">Set final options for matching and exporting the data.</p>
</div>
<div class="row marketing">
    <div class="col-md-12">
        <?php if ($has_matching): ?>
            <div id="collisions">
                <h1>Collision Handling</h1>
                <p>When two rows are determined to be a match and they have different data in other columns, these
                    settings will determine what the program does with them.</p>
                <fieldset class="form-group row">
                    <legend class="col-form-legend col-sm-2">Collision Handling</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input collision-option"
                                       type="radio"
                                       name="collision-type"
                                       id="collision-type-merge"
                                       value="merge"
                                />
                                <b>Merge Together</b>: If there is different data between the files, then merge all of
                                the data together.
                            </label>
                            <br/>
                            <div class="col-sm-11 offset-sm-1 collision-sub hidden" id="collision-sub-merge">
                                Merge together using:
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="merge_char"
                                               id="merge_both_char_new_line"
                                               value="new_line" checked="checked">
                                        A new line.
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="merge_char"
                                               id="merge_both_char_tab"
                                               value="tab">
                                        A tab character.
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="merge_char"
                                               id="merge_both_char_custom"
                                               value="custom">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">Custom</div>
                                                <input type="text" class="form-control" id="merge_custom_char"
                                                       placeholder="Custom Separator">
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input collision-option"
                                       type="radio"
                                       name="collision-type"
                                       id="collision-type-mark"
                                       value="mark"
                                />
                                <div><b>Merge and Mark</b>: Merges all of the data together like the previous option,
                                    but
                                    will
                                    surround and mark each segment of data showing which file it came from.
                                </div>
                            </label>
                            <div class="col-sm-11 offset-sm-1 collision-sub hidden" id="collision-sub-mark">
                                <p>
                                    Each segment of data will be separated by a new line and wrapped in the text below.
                                    You can customize what the data is wrapped with.
                                </p>
                                <div>You can use the following variables:
                                    <ul>
                                        <li>
                                            <code>%FILE_ID%</code> The ID of the file (if you have 3 files, the IDs will
                                            be
                                            0, 1, and 2)
                                        </li>
                                        <li><code>%FILE_NAME%</code> The name of the file.</li>
                                        <li><code>%CONTENTS%</code> The contents of the cell.</li>
                                    </ul>
                                </div>
                                <label for="marking-text">Marking Text</label>
                                <textarea class="form-control" name="marking-text" id="marking-text">[[%FILE_NAME%]] %CONTENTS%</textarea>
                                <br/>
                                <div>
                                    <h4>Example Merging</h4>
                                    <pre id="example-merge"></pre>
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input collision-option"
                                       type="radio"
                                       name="collision-type"
                                       id="collision-type-override"
                                       value="override"
                                />
                                <div><b>Override</b>: When rows match between files, one file will take precedence and
                                    only that file's data will be copied over.
                                </div>
                            </label>
                            <div class="col-sm-11 offset-sm-1 collision-sub hidden" id="collision-sub-override">
                                <p><b>File Precedence</b><br/>Click and drag the files around to change their order.
                                    The higher a file is on the list, the higher its precedence.</p>
                                <ul id="file-precedence">
                                    <?php foreach ($files as $file) {
                                        echo "<li class='btn btn-secondary form-control file-precedence' data-file='{$file['id']}'>File upload #{$file['id']}: {$file['name']}</li>\n";
                                    } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        <?php endif; ?>
        <h1>File Output</h1>
        <p>These options dictate how the merged file will be exported.</p>
        <fieldset class="form-group row">
            <legend class="col-form-legend col-sm-2">File Type</legend>
            <div class="col-sm-10">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input"
                               type="radio"
                               name="file-type"
                               id="file-type-csv"
                               value="csv"
                               checked="checked"
                        />
                        <b>CSV</b>: Outputs the file into a simple csv file.
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input"
                               type="radio"
                               name="file-type"
                               id="file-type-xlsx"
                               value="xlsx"
                        />
                        <b>Excel</b>: Generates an Excel xlsx file.
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input"
                               type="radio"
                               name="file-type"
                               id="file-type-ods"
                               value="ods"
                        />
                        <b>ODS</b>: Creates an ODF Spreadsheet (LibreOffice, OpenOffice).
                    </label>
                </div>
            </div>
        </fieldset>
        <fieldset class="form-group row">
            <legend class="col-form-legend col-sm-2">File Name</legend>
            <div class="col-sm-10">
                <?php

                $merge = [];
                foreach ($files as $file) {


                    $info = pathinfo($file['name']);
                    $file_name = basename($file['name'], '.' . $info['extension']);
                    $merge[] = $file_name;
                }
                $merge_name = join('_', $merge);


                ?>
                <div class="input-group">
                    <input type="text"
                           class="form-control"
                           placeholder="<?php echo $merge_name; ?>_merged"
                           value="<?php echo join('_', $merge); ?>_merged"
                           id="file-name"
                    />
                    <span class="input-group-addon" id="file-ext">.csv</span>
                </div>
            </div>
        </fieldset>
        <h1>Generate the Export</h1>
        <p>Once all of the options are set to your liking, it's time to actually generate the file!</p>
        <form>
            <button class="btn btn-success" id="submit_but">Click here to begin the export!</button>
            <input type="hidden" name="options_json" id="options_json"/>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {

        $("#submit_but").click(function (e) {
            var match_type = $(".collision-option:checked").val();
            var match_obj = {
                type: match_type
            };
            var opt = null;
            if (match_type == 'merge') {
                opt = $('input[name="merge_char"]:checked').val();
                if (opt == 'new_line') {
                    opt = "\n";
                } else if (opt == 'tab') {
                    opt = "\t";
                } else {
                    opt = $('#merge_custom_char').val();
                }
            } else if (match_type == 'mark') {
                opt = $('#marking-text').val();
            } else if (match_type == 'override') {
                opt = [];
                $('.file-precedence').each(function () {
                    opt.push($(this).data('file'));
                });
            } else {
                alert("Error: unknown match type " + match_type);
                e.preventDefault();
                return;
            }
            match_obj['option'] = opt;
            var file_obj = {
                ext: $('input[name="file-type"]:checked').val(),
                name: $('#file-name').val()
            };
            $('#options_json').val(JSON.stringify({
                match: match_obj,
                file: file_obj
            }));
        });

        var example_merge = function () {
            var text = $('#marking-text').val();
            var text1 = text.replace('%FILE_ID%', '1');
            text1 = text1.replace('%FILE_NAME%', 'example_file_1.xlsx');
            text1 = text1.replace('%CONTENTS%', 'Example contents things and stuff');
            var text2 = text.replace('%FILE_ID%', '1');
            text2 = text2.replace('%FILE_NAME%', 'example_file_2.csv');
            text2 = text2.replace('%CONTENTS%', 'This is from another file');
            $('#example-merge').text((text1 + "\n" + text2).trim());
        };

        $('#marking-text').bind('input propertychange', example_merge);
        example_merge();
        $('.collision-option').change(function () {
            if ($(this).is(':checked')) {
                $('#collision-sub-' + $(this).val()).show(1000);
                $('.collision-sub:not(#collision-sub-' + $(this).val() + ')').hide(1000);
            }
        });
        $('.collision-sub').each(function () {
            $(this).hide(1, function () {
                $(this).removeClass('hidden').hide();
            });
        });
        $("#file-precedence").sortable();
        $("#file-precedence li").disableSelection();

        $('#collision-type-merge').prop('checked', true);
        $('#collision-sub-merge').show(0);

        $('input[name="file-type"]').change(function () {
            var ext = $('input[name="file-type"]:checked').val();
            $('#file-ext').text('.' + ext);
        });
    });
</script>
<style>
    #sortable {
        cursor: move;
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 60%;
    }

    #sortable li {
        margin: 3px 0;
    }

    #example-merge {
        font-family: Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
        padding: 5px;
        font-size: 90%;
        color: #bd4147;
        background-color: #f7f7f9;
        border-radius: .25rem
    }

    #file-precedence {
        list-style-type: none;
        margin: 0;
        max-width: 50%;
        border: 1px solid #ccc;
        padding: 5px;
        padding-right: 15px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #file-precedence li {
        margin: 5px;
        cursor: move;
    }

    .collision-sub {
        z-index: 10;
    }

</style>