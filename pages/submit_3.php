<?php

session('options', json_decode($_POST['options_json'], true));

$str = serialize($_POST).serialize($_SESSION).serialize(microtime());
$str = md5($str);

$working = dirname(__DIR__).'/working';
while (file_exists("$working/$str")) {
    $str = md5($str.serialize(microtime()));
}
$dir = "$working/$str";
mkdir($dir, 0777, true);


$files = session('files');
foreach ($files as &$file) {
    rename($file['tmp_name'], "$dir/".basename($file['tmp_name']));
    $file['tmp_name'] = "$dir/".basename($file['tmp_name']);
    unset($file);
}
session('files', $files);

session('unique_key', $str);
session('working_dir', $dir);
session('unique_url', $url = "http://".$_SERVER['HTTP_HOST'].'/working.php?key='.$str);

file_put_contents("$dir/_session_contents", serialize($_SESSION));
session_abort();

header("Location: /working.php?key=$str");
exit();
