<?php

use Lib\RowReader;

$files = [];
for ($i = 0; $i < count($_FILES['step_1_file']['name']); $i++) {
    $file = [
        'name' => '',
        'type' => '',
        'tmp_name' => '',
        'error' => '',
        'size' => ''
    ];
    foreach ($file as $key => $value) {
        $file[$key] = $_FILES['step_1_file'][$key][$i];
    }
    $file['ext'] = pathinfo($file['name'], PATHINFO_EXTENSION);
    $filename = TMP_DIR.'/'.basename($file['tmp_name']).'.'.$file['ext'];
    @mkdir(dirname($filename), 0777, true);
    rename($file['tmp_name'], $filename);
    $file['tmp_name'] = $filename;
    $reader = new RowReader($file['tmp_name']);
    $reader->create_header();
    $file['header'] = $reader->get_header();
    $files[] = $file;
}


$master = [];
$id_count = 1;

foreach ($files as &$file) {
    $header = $file['header'];
    $mapping = [];
    foreach ($header as $value) {
        $cmp = cmp_format($value);
        if (array_key_exists($cmp, $master)) {
            $mapping[$value] = $master[$cmp];
        } else {
            $master[$cmp] = $value;
            $mapping[$value] = $value;
        }
    }
    $file['mapping'] = $mapping;
    $file['id'] = $id_count++;
    unset($file);
}

$rows = [];
foreach ($master as $key => $value) {
    $file_row = [];
    foreach ($files as $file) {
        if ($pos = array_search($value, $file['mapping'])) {
            $file_row[$file['id']] = $pos;
        } else {
            $file_row[$file['id']] = null;
        }
    }
    $rows[$key] = $file_row;
}

session('files', $files);
session('master', $master);
session('rows', $rows);