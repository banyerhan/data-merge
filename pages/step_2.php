<?php

$files = session('files');
$master = session('master');
$rows = session('rows');


$id_count = 1;

$files_cpy = $files;
foreach ($files_cpy as &$file) {
    unset($file['tmp_name'], $file['error'], $file['size'], $file['ext'], $file['type']);
}
$pretty_files = json_encode($files_cpy, JSON_PRETTY_PRINT);
$pretty_files = preg_replace('#^#ms', '        ', $pretty_files);

$master_pretty = json_encode($master, JSON_PRETTY_PRINT);
$master_pretty = preg_replace('#^#ms', '        ', $master_pretty);
?>

<script>
    step_2 = new (function () {
        var me = this;
        this.files = [];
        this.master = {};
        this.file_count = 0;
        this.rows = {};
        <?php
        echo "this.files = " . ltrim($pretty_files) . ";\n";
        echo "        this.master = " . ltrim($master_pretty) . ";\n";
        echo "        this.file_count = " . count($files) . ";\n";
        ?>

        var rename_modal = null;
        var new_row_modal = null;
        var match_modal = null;
        var id_count = 0;
        var row_template = {<?php
            echo "\n";
            $tmp = [];
            foreach ($files as $file) {
                $tmp[] = "            \"" . $file['id'] . "\": null";
            }
            echo join(",\n", $tmp) . "\n";
            ?>
        };

        var hide_row = function () {
            var row = $(this).closest('tr');
            if (row.hasClass('table-danger')) {
                row.removeClass('table-danger');
                $(this).find('.fa').first().removeClass('fa-eye-slash').addClass('fa-eye');
                $(this).removeClass('btn-outline-danger').addClass('btn-secondary');
            } else {
                row.addClass('table-danger');
                $(this).find('.fa').first().removeClass('fa-eye').addClass('fa-eye-slash');
                $(this).removeClass('btn-secondary').addClass('btn-outline-danger');
            }
        };

        var rename_row = function () {
            rename_modal = $(this);
            $('#rename-modal').modal('show');
        };

        var check_empty_rows = function () {
            $("#mapping-table").find("tbody tr:not(#new-row)").each(function () {
                if ($(this).find('.cell-value').length == 0) {
                    var me = $(this);
                    $(this).hide(500, function () {
                        me.remove();
                    });
                }
            });
        };

        var draggable_options = function (file) {
            return {
                revert: true,
                scroll: true,
                zIndex: 99999
            }
        };
        var droppable_options = function (file) {
            return {
                accept: '.cell-value[data-file="' + file + '"',
                drop: function (event, ui) {
                    var row1 = $(this).closest('tr');
                    var row2 = ui.draggable.closest('tr');
                    me.swap_rows(row1.data('key'), row2.data('key'), file);
                    check_empty_rows();
                }
            }
        };

        var match_row = function () {
            match_modal = $(this);
            $('#match-modal').modal('show');
        };

        var set_events = function () {
            $('.cell-value, .cell-null').each(function () {
                if ($(this).draggable('instance')) {
                    $(this).draggable('destroy');
                }
                if ($(this).droppable('instance')) {
                    $(this).droppable('destroy');
                }
                $(this).disableSelection();
            });
            $('.cell-value').each(function () {
                $(this).draggable(draggable_options($(this).data('file')));
            });
            $('.cell-null').each(function () {
                $(this).droppable(droppable_options($(this).data('file')));
            });
            $("button.show-hide-but").off().click(hide_row);
            $('button.rename-but').off().click(rename_row);
            $('button.match-but').off().click(match_row);
            $("#mapping-table tbody").sortable({
                handle: '.sort-handle',
                items: 'tr:not(#new-row)'
            });
        };

        this.add_row = function (row, key) {
            key = key || ("new_row_" + (++id_count));
            var tr = '<tr class="mapping-row" data-key="' + key + '" id="mapping-row-' + key + '" data-match="">';
            tr += '<td class="action-buttons">';
            tr += '<span class="btn btn-sm btn-secondary sort-handle" title="Drag to change column order"><i class="fa fa-arrows" aria-hidden="true"></i></span>';
            tr += '<button class="btn btn-sm btn-secondary show-hide-but" title="Show/Hide this row in final output"><i class="fa fa-eye" aria-hidden="true"></i></button>';
            tr += '<button class="btn btn-sm btn-secondary rename-but" title="Rename this field"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
            tr += '<button class="btn btn-sm btn-secondary match-but" title="Use to match rows"><i class="fa fa-exchange" aria-hidden="true"></i></button>';
            tr += '</td>';
            tr += '<td class="master-cell text-primary" id="master-cell-' + key + '">' + me.master[key] + '</td>';
            for (var i in row) {
                if (row.hasOwnProperty(i)) {
                    var val = row[i];
                    if (val != null) {
                        tr += '<td class="mapping-cell cell-value" data-file="' + i + '" data-is-null="no">';
                        tr += val;
                        tr += '</td>';
                    } else {
                        tr += '<td class="mapping-cell text-danger cell-null" data-file="' + i + '" data-is-null="yes">';
                        tr += '(not used)';
                        tr += '</td>';
                    }
                }
            }
            tr += "</tr>";
            tr = $(tr);
            tr.insertBefore('#new-row');
            set_events();
        };

        this.swap_rows = function (row1, row2, file) {
            row1 = $('tr[data-key="' + row1 + '"');
            row2 = $('tr[data-key="' + row2 + '"');
            var cell1 = row1.find('td[data-file="' + file + '"]');
            var cell2 = row2.find('td[data-file="' + file + '"]');

            var class1 = cell1.attr('class');
            var class2 = cell2.attr('class');

            var null1 = cell1.data('is-null');
            var null2 = cell2.data('is-null');

            var val1 = cell1.text();
            var val2 = cell2.text();

            cell1.attr('class', class2).data('is-null', null2).text(val2);
            cell2.attr('class', class1).data('is-null', null1).text(val1);
            set_events();
        };

        $(document).ready(function () {
            <?php
            echo "\n";
            foreach ($rows as $key => $row) {
                echo "            me.add_row(" . json_encode($row) . ", '$key');\n";
            }

            ?>
            $("#new-row-cell").droppable({
                accept: '.cell-value',
                drop: function (event, ui) {
                    var cell = ui.draggable;
                    new_row_modal = {
                        cell: cell
                    };
                    $("#new-row-modal").modal('show');
                }
            });
            $("#new-row-modal").on('hide.bs.modal', function () {
                new_row_modal = null;
            }).on('show.bs.modal', function () {
                var input = $("#new-row-input-name").val("").attr('placeholder', '');
                if (new_row_modal) {
                    input.attr('placeholder', new_row_modal.cell.text());
                }
            }).on('shown.bs.modal', function () {
                $("#new-row-input-name").focus();
            });
            $("#new-row-form").submit(function (e) {
                e.preventDefault();
                var val = $("#new-row-input-name").val();
                if (new_row_modal && val) {
                    var key = 'new_row_' + (++id_count);
                    me.master[key] = val;
                    me.add_row(row_template, key);
                    var cur = new_row_modal.cell.closest('tr').data('key');
                    me.swap_rows(key, cur, new_row_modal.cell.data('file'));
                }
                $("#new-row-modal").modal('hide');
            });
            $("#save-new-row-but").click(function () {
                $("#new-row-form").submit();
            });

            $("#rename-modal").on('hide.bs.modal', function () {
                rename_modal = null;
            }).on('shown.bs.modal', function () {
                $('#rename-input-name').focus();
            }).on('show.bs.modal', function () {
                var input = $('#rename-input-name').val('').attr('placeholder', '');
                if (rename_modal) {
                    var row = rename_modal.closest('tr');
                    var master = row.find('.master-cell').first();
                    input.attr('placeholder', master.text());
                }
            });
            $('#rename-form').submit(function (e) {
                e.preventDefault();
                if (rename_modal) {
                    var row = rename_modal.closest('tr');
                    var master = row.find('.master-cell').first();
                    var val = $('#rename-input-name').val();
                    if (val) {
                        master.text(val);
                    }
                }
                $('#rename-modal').modal('hide');
            });
            $('#save-rename-but').click(function () {
                $('#rename-form').submit();
            });

            $("#match-modal").on('hide.bs.modal', function () {
                match_modal = null;
            }).on('show.bs.modal', function () {
                if (match_modal) {
                    var row = match_modal.closest('tr');
                    var key = row.data('key');
                    var match = row.data('match');
                    var text = row.find('.master-cell').first().text();
                    if (match) {
                        $('#match-type-' + match).prop('checked', true);
                    } else {
                        $('#match-type-none').prop('checked', true);
                    }
                    $('#modal-title').text(text);
                }
            });
            $('#modal-save').click(function () {
                if (match_modal) {
                    var row = match_modal.closest('tr');
                    var val = $('input[name="match-type"]:checked').first().val();
                    row.data('match', val);
                    if (!val || val == 'none') {
                        row.removeClass('table-info');
                    } else {
                        row.addClass('table-info');
                    }
                }
                $('#match-modal').modal('hide');
            });
            $("#final-submit").click(function (e) {
                var rows = {};
                $("#mapping-table").find('tbody tr:not(#new-row)').each(function () {
                    var match = $(this).data('match');
                    var deleted = $(this).hasClass('table-danger');
                    var name = $(this).find('.master-cell').text();
                    var files = {};
                    $(this).find('.mapping-cell').each(function () {
                        files[$(this).data('file')] = $(this).text();
                    });
                    var key = $(this).data('key');
                    rows[key] = {
                        match: match,
                        deleted: deleted,
                        name: name,
                        files: files
                    };
                });
                $("#mapping_json").val(JSON.stringify(rows));
            });

        });

    })();
</script>
<style>
    .cell-value {
        cursor: move;
    }

    .cell-null {

    }

    .ui-droppable-hover {
        background-color: #c0ffc0;
        color: #007000 !important;
    }

    td.action-buttons .btn, .jtron-but {
        margin-left: 5px;
        padding: 2px;
        display: inline-block;
    }

    td.action-buttons .btn:first-child {
        margin-left: 0;
    }

    .jtron-but {
        margin-left: 0;
    }

    tr.bg-danger .master-cell {
        color: white !important;
        font-weight: bold;
    }

    tr.bg-danger .cell-null {
        color: white !important;
        font-style: italic;
    }

    #jtron-key {
        list-style-type: none;
    }
</style>

<div class="jumbotron">
    <h1 class="display-3">Step 2: Map Columns</h1>
    <p class="lead">Map which columns from each file combine together into the final output.</p>
    <p>Just drag and drop a cell into one that's not being used, or create a new column altogether.</p>
    <p>If you want to compare different columns together to see if they contain the same data, then click on
        the "match" icon next to any column and select comparison options.</p>
    <b>Actions:</b>
    <ul id="jtron-key">
        <li>
            <span class="btn btn-sm btn-secondary jtron-but" title="Drag to change column order">
                <i class="fa fa-arrows" aria-hidden="true"></i>
            </span> &nbsp; Click and drag this button to change the position of the columns in the final output.
        </li>
        <li>
            <button class="btn btn-sm btn-secondary jtron-but" title="Show/Hide this row in final output">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </button>
            &nbsp; This toggles whether or not this field will appear in the final output. Essentially,
            this deletes/un-deletes the field.
        </li>
        <li>
            <button class="btn btn-sm btn-secondary jtron-but" title="Rename this field">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </button>
            &nbsp; Changes the name of the field in the final output.
        </li>
        <li>
            <button class="btn btn-sm btn-secondary jtron-but" title="Use to match rows">
                <i class="fa fa-exchange" aria-hidden="true"></i>
            </button>
            &nbsp; Sets matching options for the row. If two or more rows match across different files then
            the data from those rows will be merged together.
        </li>
    </ul>
</div>
<div class="row marketing">
    <div class="col-md-12">
        <h1>Map Columns</h1>
        <table class="table table-bordered table-hover" id="mapping-table">
            <thead class="table-info">
            <th>Actions</th>
            <th>Final Output</th><?php
            echo "\n";
            foreach ($files as $file) {
                echo "    <th data-file='{$file['id']}' class='file-header'>File {$file['id']}: {$file['name']}</th>\n";
            }
            ?>
            </thead>
            <tbody>
            <tr id="new-row">
                <td>New Row</td>
                <td colspan="<?php echo count($files) + 1; ?>" class="table-success text-center" id="new-row-cell">(Drag
                    here to create a new
                    row)
                </td>
            </tr>
            </tbody>
        </table>
        <h3>Next Step: Options</h3>
        <p>Once you're satisfied with how everything looks, click the button below to move on.</p>
        <form class="form form-inline">
            <input type="hidden" id="mapping_json" name="mapping_json"/>
            <input type="submit" class="btn btn-success" value="Go on to the next section." id="final-submit"/>
        </form>
    </div>
</div>

<div class="modal fade" id="new-row-modal" tabindex="-1" role="dialog" aria-labelledby="New Row Modal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="new-row-modal-title">Create a New Row</h4>
            </div>
            <div class="modal-body">
                <form id="new-row-form">
                    <div class="form-group">
                        <label for="new-row-input-name">Enter column heading for the new row.</label>
                        <input type="text" class="form-control" id="new-row-input-name">
                        <small class="form-text text-muted">This will be the column heading in the final output file.
                        </small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="save-new-row-but">Save changes</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="rename-modal" tabindex="-1" role="dialog" aria-labelledby="New Row Modal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="rename-modal-title">Rename Row</h4>
            </div>
            <div class="modal-body">
                <form id="rename-form">
                    <div class="form-group">
                        <label for="rename-input-name">Enter new column heading for the row.</label>
                        <input type="text" class="form-control" id="rename-input-name">
                        <small class="form-text text-muted">This will be the column heading in the final output file.
                        </small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="save-rename-but">Save changes</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="match-modal" tabindex="-1" role="dialog" aria-labelledby="match modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><b id="modal-title"></b> Match Options</h4>
            </div>
            <div class="modal-body">
                <form class="form">
                    <fieldset class="form-group row">
                        <legend class="col-form-legend col-sm-2">Match Type</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="match-type" id="match-type-none"
                                           value="none" checked="checked">
                                    <b>No Match</b>: Do not use this field to combine rows from different files.
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="match-type" id="match-type-exact"
                                           value="exact">
                                    <b>Exact Match</b>: Values must match <i>exactly</i> in order to be considered the
                                    same.
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="match-type"
                                           id="match-type-case_insensitive" value="case_insensitive">
                                    <b>Case Insensitive Match</b>: This is like exact match, but case doesn't matter. In
                                    other words,
                                    <code>Joe Shmoe</code> would match <code>joe shmoe</code>, but would <i>not</i>
                                    match
                                    <code>Joe (Shmoe)</code>.
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="match-type"
                                           id="match-type-frugal" value="frugal">
                                    <b>Frugal Match</b>: The frugal match is the most likely to match text elements.
                                    Basically, it will
                                    compare fields by alphabetic letters only (a-z) and ignores case. In other words:
                                    <code>St. John's</code>
                                    would match <code>st johns</code>.
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="match-type"
                                           id="match-type-numbers_only" value="numbers_only">
                                    <b>Numbers Only</b>: This is similar to frugal match, but instead of alphabetic
                                    characters, only
                                    digits are compared. This one is useful for matching phone numbers. For example,
                                    <code>(123) 456-7890 ext. 12</code> would match
                                    <code>123-456-7890x12</code>, but would not match
                                    <code>(123) 456-7890</code>.
                                </label>
                            </div>

                            <!--                            <a data-toggle="collapse" href="#match-type-collapse" aria-expanded="false" aria-controls="collapseExample">-->
                            <!--                                What's the difference?-->
                            <!--                            </a>-->
                        </div>
                        <div class="col-sm-10 offset-sm-2 collapse" id="match-type-collapse">
                            Collapse
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>