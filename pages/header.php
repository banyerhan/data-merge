<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/ui/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/ui/jquery-ui.structure.min.css" />
    <link rel="stylesheet" type="text/css" href="/ui/jquery-ui.theme.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />

    <link rel="stylesheet" type="text/css" href="/css/style.css" />

    <script src="/js/jquery.js"></script>
    <script src="/ui/jquery-ui.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
<!--
  Created by:

  ███████╗████████╗███████╗██╗   ██╗███████╗███╗   ██╗         ██╗███████╗███████╗███████╗██████╗ ██╗███████╗███████╗ 
  ██╔════╝╚══██╔══╝██╔════╝██║   ██║██╔════╝████╗  ██║         ██║██╔════╝██╔════╝██╔════╝██╔══██╗██║██╔════╝██╔════╝ 
  ███████╗   ██║   █████╗  ██║   ██║█████╗  ██╔██╗ ██║         ██║█████╗  █████╗  █████╗  ██████╔╝██║█████╗  ███████╗ 
  ╚════██║   ██║   ██╔══╝  ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║    ██   ██║██╔══╝  ██╔══╝  ██╔══╝  ██╔══██╗██║██╔══╝  ╚════██║ 
  ███████║   ██║   ███████╗ ╚████╔╝ ███████╗██║ ╚████║    ╚█████╔╝███████╗██║     ██║     ██║  ██║██║███████╗███████║ 
  ╚══════╝   ╚═╝   ╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝     ╚════╝ ╚══════╝╚═╝     ╚═╝     ╚═╝  ╚═╝╚═╝╚══════╝╚══════╝
  -->
</head>
<body>
<nav class="navbar navbar-fixed-top navbar-light bg-faded">
    <a href="/index.php?clear=clear" class="btn btn-sm btn-warning">Start Over</a>
    &nbsp; | &nbsp;
    <?php
    $step = SEAN_PROJECT_STEP;
    $steps = [];
    $step_names = [
        'Select Files',
        'Map Columns',
        'Options'

    ];
    for ($i = 1; $i <= 3; $i++) {
        if ($step >= $i) {
            $steps[] = "<a href=\"/index.php?step=$i\" class=\"btn btn-sm btn-info\">Step $i: {$step_names[$i - 1]}</a>";
        } else {
            $steps[] = "<a href=\"#\" class=\"btn btn-sm btn-info disabled\">Step $i: {$step_names[$i - 1]}</a>";
        }
    }
    echo join(' - ', $steps);
    ?>
</nav>
<div class="container">