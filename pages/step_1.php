
<div class="jumbotron">
    <h1 class="display-3">Step 1: Select Files</h1>
    <p class="lead">Select the files that you wish to merge.</p>
</div>

<div class="row marketing">
    <div class="col-md-6 offset-md-3">
        <form id="step_1_form">
            <div id="step_1_form_more"><br /><a href="#" class="more btn btn-success">+ Add Files</a></div>
        </form>
        <br />
        <button class="next btn btn-primary" id="step_1_next" disabled="disabled">Select 2 or more files</button>


        <div class="waiting hidden">
            Form submitted. Please wait until it finishes.
        </div>
    </div>
</div>
